import { configureStore } from "@reduxjs/toolkit";
import { reducer as formReducer } from "redux-form";
import authReducer from "./slicers/authSlice";
import contactReducer from "./slicers/contactSlice";

export const store = configureStore({
    reducer: {
        auth: authReducer,
        contact: contactReducer,
        form: formReducer,
    },
});
