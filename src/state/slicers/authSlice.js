import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { accessManagement as iam } from "../../utils";
import {
    errorPayload,
    successAlertFunction,
    rejectionReducer,
} from "./slice-utils";

import { contactApi } from "../../api";
import { reset } from "redux-form";

const initialState = {
    isSignedIn:
        iam.tokenType.get() !== null &&
        iam.token.get() !== null &&
        iam.userId.get() !== null &&
        iam.tenantReference.get() !== null,
    user: null,
    error: null,
    success: null,
    status: "idle",
};

export const login = createAsyncThunk(
    "auth/login",
    async ({ formValues, navigate }, thunkAPI) => {
        try {
            const res = await contactApi.post("/auth/signin", formValues);
            const { tokenType, accessToken, id } = res.data;
            iam.tokenType.set(tokenType); // store tokenType to local storage which will be used in axios interceptor
            iam.token.set(accessToken); // store accessToken to local storage which will be used in axios interceptor
            iam.userId.set(id); // set user so that later getCurrentUser thunk can access the user's details

            const userRes = await contactApi.get(`/getUserState/id/${id}`);
            const userDetails = userRes.data.data;
            const { tenantReference } = userDetails;

            iam.tenantReference.set(tenantReference); // store tenantReference to local storage which will be used in axios interceptor

            thunkAPI.dispatch(reset("loginForm")); // reset loginForm state in store
            navigate("/contacts"); // navigate user to contact list page
            return { data: userDetails, message: "Successfully Login" };
        } catch (err) {
            console.log(err);
            return errorPayload(err, thunkAPI, "ERROR: Unable to Login");
        }
    }
);

export const getCurrentUser = createAsyncThunk(
    "auth/getCurrentUser",
    async (navigate, thunkAPI) => {
        // This will be true if all access credentials are in local storage
        // this will allow auto login operation
        const isAutoLogin = thunkAPI.getState().auth.isSignedIn;

        if (isAutoLogin) {
            try {
                const res = await contactApi.get(
                    `/getUserState/id/${iam.userId.get()}`
                );
                const userDetails = res.data.data;
                navigate("/contacts"); // navigate user to contact list page
                return { data: userDetails, message: "Successfully Login" };
            } catch (err) {
                return errorPayload(err, thunkAPI, "ERROR: Unable to Login");
            }
        }
        return thunkAPI.rejectWithValue("Logging Out");
    }
);

const authSlice = createSlice({
    name: "auth",
    initialState,
    reducers: {
        cleanAuth: (state) => {
            iam.removeAll();
            state = initialState;
        },
    },
    extraReducers: {
        [login.pending]: (state) => {
            state.status = "pending";
        },
        [getCurrentUser.pending]: (state) => {
            state.status = "pending";
        },
        [login.rejected]: rejectionReducer,
        [getCurrentUser.rejected]: rejectionReducer,
        [login.fulfilled]: (state, action) => {
            state.isSignedIn = true;
            state.user = action.payload.data;
            successAlertFunction(state, action);
        },
        [getCurrentUser.fulfilled]: (state, action) => {
            state.isSignedIn = true;
            state.user = action.payload.data;
            successAlertFunction(state, action);
        },
    },
});

export const { cleanAuth } = authSlice.actions;

export const logout = (navigate) => (dispatch) => {
    dispatch(cleanAuth()); // when logging out the auth data will be cleaned up
    navigate("/"); // navigate to login page
};

export default authSlice.reducer;
