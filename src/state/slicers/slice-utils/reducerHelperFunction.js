// Helper function that will return the error props as payload
export const errorPayload = (err, thunkAPI, message = null) => {
    if (message) {
        return thunkAPI.rejectWithValue(message);
    }
    if (err.response)
        return thunkAPI.rejectWithValue(err.response.data.message);

    return thunkAPI.rejectWithValue(err.message);
};

// helper call back function that will handle any reducer state mutation
// for rejected async thunk
export const rejectionReducer = (state, action) => {
    if (state.success) state.success = null; // set success alert to null if it is not null
    // action's payload will be the message of the error that is being dispatched
    state.error = {
        severity: "error", // this will be used as the color of the snackbar alert
        message: action.payload, // used as the snackbar's message
        open: true, // this will be used as a prop to indicate that the snackbar would be opened
    };
    state.status = "error";
};

// helper function that will mutate alert state in contact reducer
// for any successful process/requests
export const successAlertFunction = (state, action) => {
    if (state.error) state.error = null;
    state.success = {
        severity: "success",
        message: action.payload.message,
        open: true,
    };
    state.status = "success";
};
